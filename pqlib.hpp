#pragma once
#include <vector>
#include <functional>
#include <map>
#include <tuple>
#include <cmath>
#include <gmp.h>

struct tableRow {
/// @todo write the constructor for init
// one row of table for nm output (see actual task)
  //  tableRow(mpq_t ai, mpz_t qi, mpz_t Pi, mpz_t Qi, mpz_t Ai, mpz_t Bi) : ai(ai), qi(qi), Pi(Pi), Qi(Qi), Ai(Ai), Bi(Bi) {}
    mpq_t ai;
    mpz_t qi;
    mpz_t Pi;
    mpz_t Qi;
    mpz_t Ai;
    mpz_t Bi;
    

    //std::tuple<> get_tuple() {
    //    return std::tuple<> ();}


  //  friend std::ostream& operator<< (std::ostream& os, const tableRow& row) {
   //     os << " ai = " << ai << " qi = " << qi << " Pi = " << Pi << " Qi = " << Qi << " Ai = " << Ai << " Bi = " << Bi;
   //     return os;
   // }
};

typedef std::vector<tableRow> resultTable; // table for output




//namespace utils {
//} // namespace utils
