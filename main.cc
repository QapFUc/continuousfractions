#define _OPEN_SYS_ITOA_EXT
#include <gmp.h>
#include <fstream>
#include <iostream>
#include <math.h>
#include <vector>
#include "math.h"
#include <stdio.h>
#include <stdlib.h>
#include <functional>
#include <algorithm>
#include <cstring>

unsigned long long LengthPeriod(const char* value) {
    //resultTable cont;
    mpf_t D;
    mpf_t Ai0;
    mpf_t Ai1;
    mpf_t Ai2;
    mpf_t sqrtD;
    mpf_t qi0;
    mpf_t qi1;
    mpf_t Pi0;
    mpf_t Pi1;
    mpf_t Qi0;
    mpf_t Qi1;
    mpf_t ai;
    mpf_t Bi0;
    mpf_t Bi1;
    mpf_t Bi2;
    mpf_t mult;
    mpf_t sum;
    mpf_t stop;
    double one, zero, cell, fr, mr;
    unsigned long long i = 0;
    one = 1.0;
    zero = 0.0;

    mpf_set_default_prec (6);
    mpf_init(Ai0);
    mpf_init(stop);
    mpf_init(Ai1);
    mpf_init(Ai2);
    mpf_init(Bi0);
    mpf_init(Bi1);
    mpf_init(Bi2);
    mpf_init (qi1);
    mpf_init(D);
    mpf_init(Pi0);
    mpf_init(Pi1);
    mpf_init(Qi0);
    mpf_init(Qi1);
    mpf_init(qi0);
    mpf_init(mult);
    mpf_init(ai);
    mpf_init(sum);
    mpf_init(sqrtD);

    mpf_set_str (D, value, 10);
    mpf_sqrt(sqrtD,D);
    mpf_set_d(Pi0,zero);
    mpf_set_d(Pi1,zero);
    mpf_set_d(Qi0,one);
    mpf_set_d(Qi1,one);
    mpf_set_d(Ai1,1);
    mpf_set_d(Ai2,0);
    mpf_set_d(Bi1,0);
    mpf_set_d(Bi2,1);

    mpf_add(sum,Pi0,sqrtD);
    mpf_div(ai,sum,Qi0);
    fr = mpf_get_d(ai);
    mr = modf(fr, &cell);
    mpf_set_d(stop,cell);
    mpf_add(stop,stop,stop);
    
    while (mpf_cmp(stop,qi0)) {
        // qi and ai
        mpf_add(sum,Pi0,sqrtD);
        mpf_div(ai,sum,Qi0);
        fr = mpf_get_d(ai);
        mr = modf(fr, &cell);
        mpf_set_d(qi0,cell);
        mpf_set(qi1,qi0);

        //Ai
        mpf_mul(mult,Ai1,qi0);
        mpf_add(Ai0,mult,Ai2);
        mpf_set(Ai2,Ai1);
        mpf_set(Ai1,Ai0);

        //Bi
        mpf_mul(mult,Bi1,qi0);
        mpf_add(Bi0,mult,Bi2);
        mpf_set(Bi2,Bi1);
        mpf_set(Bi1,Bi0);

#if !defined(SUPER_SILENT) 
#if !defined(SILENT)
        std::cout<< "i : " << i << std::endl;
        gmp_printf ("ai  :       %Ff\n\n", ai);
        gmp_printf ("qi :       %Ff\n\n", qi0);
        gmp_printf ("stop :      %Ff\n\n", stop);
        //gmp_printf ("qi-1 :       %Ff\n\n", qi1);
        //gmp_printf ("Pi :       %Ff\n\n", Pi0);
        //gmp_printf ("Pi-1 :       %Ff\n\n", Pi1);
        //gmp_printf ("Qi :       %Ff\n\n", Qi0);
        //gmp_printf ("Qi-1 :       %Ff\n\n", Qi1);
        //gmp_printf ("Ai :       %Ff\n\n", Ai0);
        //gmp_printf ("Ai-1 :       %Ff\n\n", Ai1);
        //gmp_printf ("Ai-2 :       %Ff\n\n", Ai2);
        //gmp_printf ("Bi :       %Ff\n\n", Bi0);
        //gmp_printf ("Bi-1 :       %Ff\n\n", Bi1);
      //  gmp_printf ("Bi-2 :       %Ff\n\n", Bi2);
#endif
#if defined(SILENT)
      std::cout<< "i : " << i << std::endl;
      gmp_printf ("ai  :       %Ff\n\n", ai);
      //gmp_printf ("A : %Ff\n\n", Ai0);
      //gmp_printf ("B : %Ff\n\n", Bi0);
      gmp_printf ("qi :       %Ff\n\n", qi0);
      gmp_printf ("stop :      %Ff\n\n", stop); 
#endif    
#endif   

    //gmp_printf ("Square root: %.200Ff\n\n", Ai0);
        //Pi
        mpf_mul(mult,qi1,Qi1);
        mpf_sub(Pi0,mult,Pi1);
        mpf_set(Pi1,Pi0);

        //Qi
        mpf_mul(mult,Pi0,Pi0);
        mpf_sub(sum,D,mult);
        mpf_div(Qi0,sum,Qi1);
        mpf_set(Qi1,Qi0);

        i++;
    }
    return i-1;
}

unsigned long long LengthPeriod(const unsigned long long& value) {
    std::string value_str;
    value_str = std::to_string(value);
    char *cstr = new char[value_str.length() + 1];
    strcpy(cstr, value_str.c_str());
    mpf_t D;
    mpf_t Ai0;
    mpf_t Ai1;
    mpf_t Ai2;
    mpf_t sqrtD;
    mpf_t qi0;
    mpf_t qi1;
    mpf_t Pi0;
    mpf_t Pi1;
    mpf_t Qi0;
    mpf_t Qi1;
    mpf_t ai;
    mpf_t Bi0;
    mpf_t Bi1;
    mpf_t Bi2;
    mpf_t mult;
    mpf_t sum;
    mpf_t stop;
    double one, zero, cell, fr, mr;
    unsigned long long i = 0;
    one = 1.0;
    zero = 0.0;

    mpf_set_default_prec (6);
    mpf_init(Ai0);
    mpf_init(stop);
    mpf_init(Ai1);
    mpf_init(Ai2);
    mpf_init(Bi0);
    mpf_init(Bi1);
    mpf_init(Bi2);
    mpf_init (qi1);
    mpf_init(D);
    mpf_init(Pi0);
    mpf_init(Pi1);
    mpf_init(Qi0);
    mpf_init(Qi1);
    mpf_init(qi0);
    mpf_init(mult);
    mpf_init(ai);
    mpf_init(sum);
    mpf_init(sqrtD);

    mpf_set_str (D, cstr, 10);
    delete [] cstr;
    mpf_sqrt(sqrtD,D);
    mpf_set_d(Pi0,zero);
    mpf_set_d(Pi1,zero);
    mpf_set_d(Qi0,one);
    mpf_set_d(Qi1,one);
    mpf_set_d(Ai1,1);
    mpf_set_d(Ai2,0);
    mpf_set_d(Bi1,0);
    mpf_set_d(Bi2,1);

    mpf_add(sum,Pi0,sqrtD);
    mpf_div(ai,sum,Qi0);
    fr = mpf_get_d(ai);
    mr = modf(fr, &cell);
    mpf_set_d(stop,cell);
    mpf_add(stop,stop,stop);
    
    while (mpf_cmp(stop,qi0)) {
        // qi and ai
        mpf_add(sum,Pi0,sqrtD);
        mpf_div(ai,sum,Qi0);
        fr = mpf_get_d(ai);
        mr = modf(fr, &cell);
        mpf_set_d(qi0,cell);
        mpf_set(qi1,qi0);

        //Ai
        mpf_mul(mult,Ai1,qi0);
        mpf_add(Ai0,mult,Ai2);
        mpf_set(Ai2,Ai1);
        mpf_set(Ai1,Ai0);

        //Bi
        mpf_mul(mult,Bi1,qi0);
        mpf_add(Bi0,mult,Bi2);
        mpf_set(Bi2,Bi1);
        mpf_set(Bi1,Bi0);

#if !defined(SUPER_SILENT) 
#if !defined(SILENT)
        std::cout<< "i : " << i << std::endl;
        gmp_printf ("ai  :       %Ff\n\n", ai);
        gmp_printf ("qi :       %Ff\n\n", qi0);
        gmp_printf ("stop :      %Ff\n\n", stop);
        //gmp_printf ("qi-1 :       %Ff\n\n", qi1);
        //gmp_printf ("Pi :       %Ff\n\n", Pi0);
        //gmp_printf ("Pi-1 :       %Ff\n\n", Pi1);
        //gmp_printf ("Qi :       %Ff\n\n", Qi0);
        //gmp_printf ("Qi-1 :       %Ff\n\n", Qi1);
        //gmp_printf ("Ai :       %Ff\n\n", Ai0);
        //gmp_printf ("Ai-1 :       %Ff\n\n", Ai1);
        //gmp_printf ("Ai-2 :       %Ff\n\n", Ai2);
        //gmp_printf ("Bi :       %Ff\n\n", Bi0);
        //gmp_printf ("Bi-1 :       %Ff\n\n", Bi1);
      //  gmp_printf ("Bi-2 :       %Ff\n\n", Bi2);
#endif

#if defined(SILENT)
      std::cout<< "i : " << i << std::endl;
      gmp_printf ("ai  :       %Ff\n\n", ai);
      //gmp_printf ("A : %Ff\n\n", Ai0);
      //gmp_printf ("B : %Ff\n\n", Bi0);
      gmp_printf ("qi :       %Ff\n\n", qi0);
      gmp_printf ("stop :      %Ff\n\n", stop); 
#endif    
#endif   

    //gmp_printf ("Square root: %.200Ff\n\n", Ai0);
        //Pi
        mpf_mul(mult,qi1,Qi1);
        mpf_sub(Pi0,mult,Pi1);
        mpf_set(Pi1,Pi0);

        //Qi
        mpf_mul(mult,Pi0,Pi0);
        mpf_sub(sum,D,mult);
        mpf_div(Qi0,sum,Qi1);
        mpf_set(Qi1,Qi0);

        i++; 
    }
    return i-1;
}

bool Prime(long long n) {
    for (long long i = 2; i <= sqrt(n); i++) {
        if (n % i == 0)
            return false;
    }
    return true;
}

bool SqrSum(const unsigned long long &number) {
    unsigned int sqrt = std::round(std::sqrt(number));
    for(unsigned int i = 1; i < sqrt; i++) {
            for(unsigned int j = 1; j*j <= number - i*i; j++) {
                if (i*i + j*j == number) {
                    std::cout << number << '=' << i << "^2 + " << j << "^2" << std::endl;
                    return true;
                }
        }
    }
    return false;
}
bool RealSqrt(const unsigned long long& number) {
    unsigned long long rounded_sqrt = std::round(std::sqrt(number));
    if (rounded_sqrt * rounded_sqrt == number) {
        return true;
    }
    return false;
}
bool FreeSqrt(const unsigned long long &number) {
    for(unsigned long i = 2; i*i<number; i++){
        if ((number % (i*i)) == 0){
            return false;
        }
    }
    return true;
}

unsigned long long pow(int a, int b){
    unsigned long long c = 1;
    for(int i = 0; i < b ; i++){
        c = c * a;
    }
    return c;
}

int main(int argc, char **argv) {
    std::ofstream out;
    out.open("data.txt");
    unsigned long long perold = 1;
    unsigned long long per = 1;
    for (long long b = 2; b < 100000000; b++) {
        if(Prime(b) && (RealSqrt(b) == 0)) {
            for(int n = 0;n<9; n= n+1){ 
                perold = per;
                per = LengthPeriod(pow(b,2*n+1));
                out<<"i = "<< 2*n+1<< ", b^2*n+1 = "<< pow(b,2*n+1) <<", l = "<<per<< ", i mod b = " << (2*n+1)%b <<", l mod b = "<< per % b<< ", l(pow(b,i))/pow(b,n) = " <<  per / std::pow(b,n) << std::endl;
            }
        }
    }
    out.close();
   return 0;
}
